﻿import App from './src/javascript/app';
import './src/styles/styles.css';

let app = new App();//запуск игры

export default function () {
  App.rootElement.innerHTML = '';
  app = null;
  app = new App();
}