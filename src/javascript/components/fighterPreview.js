﻿import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (!fighter) return '';
  const fighterInfo = createInfo(fighter)
  const fighterImage = createFighterImage(fighter);

  fighterElement.append(fighterImage, fighterInfo);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const fighterInfo = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  fighterInfo.textContent = Object.keys(fighter).map(key => {
    if (key === '_id' || key === 'source') {
      return null;
    };
    return `${key}: ${fighter[key]} \n`
  }
  ).filter(el => el).join('');

  return fighterInfo;
}