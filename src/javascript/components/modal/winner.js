﻿import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import index from '../../../../index';

export function showWinnerModal(fighter) {
  // call showModal function 
  const title = `${fighter.name}'s winned!`;
  const bodyElement = createFighterImage(fighter);
  const onClose = () => {
    index();
  };

  showModal({ title, bodyElement, onClose});
};


function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
    style: {
      display: 'block'
    }
  };

  const imgElement = createElement({
    tagName: 'img',
    className: '',
    attributes,
  });

  return imgElement;
}

