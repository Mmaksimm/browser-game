﻿import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const fighterNotDefender = {
      defense: 0
    };

    const controlKeys = []
    Object.keys(controls).forEach(control => {
      if (typeof controls[control] === 'string') {
        controlKeys.push(controls[control])
        return;
      }
      controlKeys.push(...controls[control])
      return;
    });

    const controlKeysCriticalHitCombination = []
    Object.keys(controls).forEach(control => {
      if (typeof controls[control] !== 'string') {
        controlKeysCriticalHitCombination.push(...controls[control])
      }
    });

    class Player {
      constructor(criticalHitCombination, fighter, figterPosition) {
        this.figterPosition = figterPosition;
        this.halthBarId = figterPosition === 'left'
          ? document.getElementById('left-fighter-indicator')
          : document.getElementById('right-fighter-indicator');
        this.fighter = fighter;
        this.fighterHealth = this.fighter.health;
        this.attack = false;
        this.block = false;
        this.criticalHitCombination = {};
        this.criticalHitCombinationNotAllowed = false
        criticalHitCombination.forEach(key => {
          this.criticalHitCombination[key] = false
        })
      }
      criticalHitCombinationOk() {
        return !Object.values(this.criticalHitCombination).includes(false)
      }
      keysDownCriticalHitCombination() {
        return Object.values(this.criticalHitCombination).includes(true)
      }
      keysDownAttackBlockOk() {
        if (this.attack === true || this.block === true) {
          return true;
        };
        return false;
      }
      keysDownOk() {
        if (this.keysDownCriticalHitCombination()) {
          return true
        };
        if (this.keysDownAttackBlockOk()) {
          return true;
        }
      }
      criticalHitCombinationNotAllowedSet() {
        this.criticalHitCombinationNotAllowed = true;
        setTimeout(() => {
          this.criticalHitCombinationNotAllowed = false;
        }, 10000)
      }
      calculationAndRenderHalthBar(damage) {
        this.fighterHealth = (this.fighterHealth - damage) >= 0
          ? this.fighterHealth - damage
          : 0;
        this.halthBarId.style.width = (this.fighterHealth / this.fighter.health * 100) + '%';
        return this.fighterHealth
      }
    }

    const playerOne = new Player(controls.PlayerOneCriticalHitCombination, firstFighter, 'left');
    const playerTwo = new Player(controls.PlayerTwoCriticalHitCombination, secondFighter, 'right');

    const keydown = event => controlKeysСheck(event);
    const keyup = event => controlKeysСheck(event)
    
    document.addEventListener('keydown', keydown);
    document.addEventListener('keyup', keyup);

    function controlKeysСheck(event) {
      return controlKeys.includes(event.code)
        ? keyDownUpCheck(event)
        : '';
    };

    function keyDownUpCheck(event) {
      if (event.type === 'keydown') {
        keyDown(event.code);
      };
      if (event.type === 'keyup') {
        keyUp(event.code);
      }
    };

    function keyUp(keyCode) {
      if (controlKeysCriticalHitCombination.includes(keyCode)) {
        controlKeysCriticalHitCombinationUp(keyCode) 
      }
      controlKeysUp(keyCode);
    };

    function keyDown(keyCode) {
      if (controlKeysCriticalHitCombination.includes(keyCode)) {
        controlKeysCriticalHitCombinationDown(keyCode); 
      }
      controlKeysDown(keyCode);
    };

    
    function controlKeysDown(keyCode) {
      switch (keyCode) {
        case controls.PlayerOneBlock:
          if (playerOne.keysDownOk()) break
          playerOne.block = true;
          break;
        case controls.PlayerTwoBlock:
          if (playerTwo.keysDownOk()) break
          playerTwo.block = true;
        default:
          break;
      };

      switch (keyCode) {
        case controls.PlayerOneAttack:
          if (playerOne.keysDownOk()) break;
          if (playerTwo.attack) break;
          playerOne.attack = true;
          attackEvent(playerOne, playerTwo);
          break;
        case controls.PlayerTwoAttack:
          if (playerTwo.keysDownOk()) break;
          if (playerOne.attack) break
          playerTwo.attack = true;
          attackEvent(playerTwo, playerOne);
        default:
          break;
      };
    };

    function controlKeysUp(keyCode) {
      switch (keyCode) {
        case controls.PlayerOneBlock:
          playerOne.block = false;
          break;
        case controls.PlayerTwoBlock:
          playerTwo.block = false;
          break;
        case controls.PlayerOneAttack:
          playerOne.attack = false;
          break;
        case controls.PlayerTwoAttack:
          playerTwo.attack = false
          break
        default:
          break;
      }
    };

    function attackEvent(attacker, defender) {
      const defenderFighter = defender.block
        ? defender.fighter
        : fighterNotDefender;

      const damage = getDamage(attacker.fighter, defenderFighter);


      if (!defender.calculationAndRenderHalthBar(damage)) {
        playEnd(attacker)
      };
    };


    function controlKeysCriticalHitCombinationDown(keyCode) {
      if (controls.PlayerOneCriticalHitCombination.includes(keyCode)) {

        if (playerOne.keysDownAttackBlockOk()) return;
        if (playerOne.criticalHitCombination[keyCode]) return;
        playerOne.criticalHitCombination[keyCode] = true;
        if (!playerOne.keysDownCriticalHitCombination()) return;
        if (playerOne.criticalHitCombinationNotAllowed) return;
        CriticalHitCombinationEvent(playerOne, playerTwo)
        return;
      };
      if (controls.PlayerTwoCriticalHitCombination.includes(keyCode)) {
        if (playerTwo.keysDownAttackBlockOk()) return;
        if (playerTwo.criticalHitCombination[keyCode]) return;
        playerTwo.criticalHitCombination[keyCode] = true;
        if (!playerTwo.keysDownCriticalHitCombination()) return;
        if (playerTwo.criticalHitCombinationNotAllowed) return;
        CriticalHitCombinationEvent(playerTwo, playerOne)
        return;
      };
      return;
    }


    function controlKeysCriticalHitCombinationUp(keyCode) {
      if (controls.PlayerOneCriticalHitCombination.includes(keyCode)) {
        playerOne.criticalHitCombination[keyCode] = false;
        return;
      };
      if (controls.PlayerTwoCriticalHitCombination.includes(keyCode)) {
        playerTwo.criticalHitCombination[keyCode] = false;
        return;
      };
      return;
    }


    function CriticalHitCombinationEvent(attacker, defender) {
      attacker.criticalHitCombinationNotAllowedSet()
      const damage = 2 * attacker.fighter.attack;

      if (!defender.calculationAndRenderHalthBar(damage)) {
        playEnd(attacker)
      };
    };

    function playEnd(attacker) {
      document.removeEventListener('keydown', keydown);
  document.removeEventListener('keyup', keyup);

      const winner = attacker.figterPosition === 'left'
        ? firstFighter
        : secondFighter
      resolve(winner);
    }
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  const power = defense * dodgeChance;
  return power;
}
